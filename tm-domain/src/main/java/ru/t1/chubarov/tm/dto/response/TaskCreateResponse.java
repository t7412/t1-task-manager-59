package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCreateResponse extends AbstractTaskResponse {

    @Nullable
    private TaskDTO task;

    public TaskCreateResponse(@Nullable final TaskDTO task) {
        this.task = task;
    }

}
