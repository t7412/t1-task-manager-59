package ru.t1.chubarov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.chubarov.tm.event.ConsoleEvent;

@Component
public final class ApplicationAboutListener extends AbstractSystemListener {

    @Override
    @EventListener(condition = "@applicationAboutListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ABOUT]");
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        System.out.println("name: " + systemEndpoint.getAbout(request).getName());
        System.out.println("e-mail: " + systemEndpoint.getAbout(request).getEmail());
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show about program.";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

}
