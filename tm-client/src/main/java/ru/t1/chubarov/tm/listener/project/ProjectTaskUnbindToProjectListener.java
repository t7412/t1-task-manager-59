package ru.t1.chubarov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.ProjectTaskUnbindToProjectRequest;
import ru.t1.chubarov.tm.event.ConsoleEvent;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

@Component
public final class ProjectTaskUnbindToProjectListener extends AbstractProjectListener {

    @Override
    @EventListener(condition = "@projectTaskUnbindToProjectListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[UNBIND TASK FROM PROJECT");
        System.out.println("ENTER PROJECT ID: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID: ");
        @NotNull final String taskId = TerminalUtil.nextLine();

        @NotNull final ProjectTaskUnbindToProjectRequest request = new ProjectTaskUnbindToProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        projectEndpoint.unbindTaskToProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-unbind-from-project";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Unbind task from project bi id.";
    }

}
