package ru.t1.chubarov.tm.api.property;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseUsername();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHbm2DDL_Auto();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getDatabaseUseSecond();

    @NotNull
    String getDatabaseUseQueryCache();

    @NotNull
    String getDatabaseMinimalPuts();

    @NotNull
    String getDatabaseRegionPrefix();

    @NotNull
    String getDatabaseRegionFactory();

    @NotNull
    String getDatabaseProviderConfig();

}
